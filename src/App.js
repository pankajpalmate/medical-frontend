import { useEffect, useState } from "react";
import { Routes, Route, useLocation, useNavigate } from "react-router-dom";
import AddVideos from "./pages/AddVideos";
import AdminList from "./pages/AdminList";
import Dashboard from "./pages/Dashboard";
import UserList from "./pages/UserList";
import Header from "./sharedComponent/Header";
import Login from "./sharedComponent/Login";
import SideBar from "./sharedComponent/SideBar";

function App() {
  const location = useLocation().pathname;
  const navigate = useNavigate();

  
  
  return (
    <>
      {location != "/" && (
        <>
          <SideBar />
          <Header  />
        </>
      )}

      <Routes>
        <Route path="/" element={<Login/>} />
        <Route path="dashboard" element={<Dashboard />} />
        <Route path="AddVideos" element={<AddVideos />} />
        <Route path="AdminList" element={<AdminList />} />
        <Route path="UserList" element={<UserList />} />
      </Routes>
    </>
  );
}

export default App;
