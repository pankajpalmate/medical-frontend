import React, { useContext, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "../assets/css/cx-sidebar.css";
import "../assets/css/style.css";
import logo from "../assets/images/Sidebar/glimmer_logo.svg";
import glimmer_small from "../assets/images/Sidebar/glimmer_small.svg";
import { AppContext } from "../context/AppContext";
import ic_hamburger from "../assets/images/Sidebar/ic_hamburger.svg";

export default function SideBar() {
  const logout = (e) => {
    // e.preventDefault();
    localStorage.removeItem("user_id");
    localStorage.removeItem("auth_token");
    localStorage.removeItem("userDetails");
    localStorage.clear();
   
    window.location.href='/'
  };
  const location = useLocation();
  let currentRoute = location.pathname;
  const { sidebar, setSidebar, Opensidebar, setOpensidebar, setLoggedIn } =
    useContext(AppContext);

  useEffect(() => {
    console.log("Opensidebar", Opensidebar);
  }, [Opensidebar]);

  const handleSidebar = () => {
    setSidebar(!sidebar);
  };

  return (
    <div>
      <aside
        id="cx-sidebar"
        className={`${
          sidebar ? "cx-active " : Opensidebar ? "cx-sidbar-open" : ""
        }`}
      >
        <div
          className={
            sidebar ? "cx-sidebar-brand cx-active " : "cx-sidebar-brand"
          }
        >
          <a href="#">
            {/* <img src={ic_hamburger} alt="" /> */}
            {sidebar ? (
              <img
                className="cx-brand-icon cx-change"
                src={glimmer_small}
                alt=""
                style={{ width: "40px" }}
              />
            ) : (
              <img className={"cx-brand-icon"} src={logo} />
            )}
          </a>
        </div>
        <div
          className={
            sidebar ? "cx-sidebar-toggler cx-active" : "cx-sidebar-toggler"
          }
          id="cx-sidebar-toggler"
        >
          <button
            type="button"
            className="cxd-btn d-flex align-items-center"
            onClick={handleSidebar}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              style={{ fill: "rgba(0, 0, 0, 1)" }}
            >
              <path d="m13.061 4.939-2.122 2.122L15.879 12l-4.94 4.939 2.122 2.122L20.121 12z"></path>
              <path d="M6.061 19.061 13.121 12l-7.06-7.061-2.122 2.122L8.879 12l-4.94 4.939z"></path>
            </svg>
          </button>
        </div>
        <ul className={sidebar ? "cx-list cx-active " : "cx-list"}>
          <li>
            <Link
              to="/dashboard"
              className={
                currentRoute === "/dashboard" ? "active-menu-sidebar" : ""
              }
            >
              <div className="cx-list-c">
                <i className="bx bxs-category"></i>
                <span>Dashboard</span>
              </div>
            </Link>

            {/* <div className="cx-tooltip">Dashboard</div> */}
          </li>
          <li>
            <Link
              to="/AdminList"
              className={
                currentRoute === "/AdminList" ? "active-menu-sidebar" : ""
              }
            >
              <div className="cx-list-c">
                <i className="bx bxs-briefcase"></i>
                <span>Admin</span>
              </div>
            </Link>
            {/* <div className="cx-tooltip">Users</div> */}
          </li>
          <li>
            <Link
              to="/UserList"
              className={
                currentRoute === "/UserList" ? "active-menu-sidebar" : ""
              }
            >
              <div className="cx-list-c">
                <i className="bx bxs-user"></i>
                <span>Users</span>
              </div>
            </Link>
            {/* <div className="cx-tooltip">Users</div> */}
          </li>
          <li>
            <Link
              to="#"
              // className={
              //   currentRoute === "/UserList" ? "active-menu-sidebar" : ""
              // }
            >
              <div className="cx-list-c" onClick={() => logout()}>
                <i className="bx bxs-user"></i>
                <span>Logout</span>
              </div>
            </Link>
            {/* <div className="cx-tooltip">Users</div> */}
          </li>
        </ul>
      </aside>
    </div>
  );
}
