import React, { useState, useContext } from "react";
import "../assets/css/cx-navbar.css";
import "../assets/css/style.css";
import profile from "../assets/images/Sidebar/profile_pic.png";
import { AppContext } from "../context/AppContext";
import ic_hamburger from '../assets/images/Sidebar/ic_hamburger.svg'


export default function Header() {
  const { sidebar, setSidebar, Opensidebar, setOpensidebar } = useContext(AppContext);


const handlemobileSidebar = () =>{
setOpensidebar(!Opensidebar)
console.log(Opensidebar);
}
const logout = ()=>{


  localStorage.clear();
  window.location.href=''
}

  const handleSidebar = () => {
    setSidebar(!sidebar);
    console.log(sidebar);
  };

  return (
    <header className={sidebar ? "cx-active" : ""}>
      <nav className="navbar navbar-expand-lg cx-navbar">
        <div className="container-fluid">
        <img src={ic_hamburger} alt="" className="mobile-bars" onClick={handlemobileSidebar} />
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="cx-bread-crumb me-auto">
            <nav className="d-flex align-items-center">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="#">Curriculums</a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Pre-Primary
                </li>
              </ol>
            </nav>
          </div>

          <div className="cx-account ms-auto">
            <img src={profile} />
          </div>
        </div>
      </nav>

      <div className="cx-account-popup">
        <div className="cx-account-in">
          <ul>
            <li>
              <a href="#">
                <i className="bx bx-user"></i>My Account
              </a>
            </li>
            <li>
              <a href="#" onClick={logout}>
                <i className="bx bx-log-out-circle"></i>Logout
              </a>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
}
