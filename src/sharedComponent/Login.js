import React, { useState } from "react";
import "../assets/css/cx-login.css";
import "../assets/css/style.css";
import { useNavigate } from "react-router-dom";
import emai_icon from "../assets/images/login/email.svg";
import password_icon from "../assets/images/login/password.svg";
import login_logo from "../assets/images/login/glimmer_logo_white.svg";
import logo from "../assets/images/Sidebar/glimmer_logo.svg";
import swal from "sweetalert";
import { postWithAuthCallWithErrorResponse } from "../api/ApiServices";
import ApiConfig from "../api/ApiConfig";

export default function Login({ setLoggedIn }) {
  const navigate = useNavigate();
  const [userDetails, setUserDetails] = useState({
    email: "",
    password: "",
    login_mode: "web",
    ip: "",
  });
  const [loading, setLoading] = useState(false);
  const [errMsg, setErrMsg] = useState({ email: "", password: "" });
  // const navigate = useNavigate();
 
  return (
    <main style={{ height: "100vh" }}>
      <div className="cx-login">
        <div className="row gx-0">
          <div className="col-md-6">
            <div className="cx-login-graphic">
              <div className="cx-login-text">
                <img src={login_logo} alt="glimmer_logo" />
                <p>Endless possibilities with Glimmer.</p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="cx-login-form">
              <div className="cx-login-content d-flex justify-content-center flex-column">
                <div className="cx-login-content-in">
                  <div className="cx-brand">
                    <img className="cx-brand-img" src={logo} alt="logo" />
                  </div>

                  {/* <form> */}
                  <div className="mb-4 position-relative">
                    <label htmlFor="cx-mail" className="form-label">
                      Email
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="cx-mail"
                      placeholder="name@example.com"
                      value={userDetails.email}
                      onChange={(e) => {
                        setErrMsg({ ...errMsg, email: "" });
                        setUserDetails({
                          ...userDetails,
                          email: e.target.value,
                        });
                      }}
                    />
                    <img
                      className="cx-input-icon"
                      src={emai_icon}
                      alt="login-email"
                    />
                  </div>
                  {errMsg.email.length > 0 && (
                    <span className="text-danger">{errMsg.email}</span>
                  )}
                  <div className="mb-4 position-relative">
                    <label htmlFor="cx-pwd" className="form-label">
                      Password
                    </label>
                    <input
                      type="password"
                      className="form-control"
                      id="cx-pwd"
                      placeholder=""
                      value={userDetails.password}
                      onChange={(e) => {
                        setErrMsg({ ...errMsg, password: "" });
                        setUserDetails({
                          ...userDetails,
                          password: e.target.value,
                        });
                      }}
                    />
                    <img
                      className="cx-input-icon"
                      src={password_icon}
                      alt="password-email"
                    />
                  </div>
                  {errMsg.password.length > 0 && (
                    <span className="text-danger">{errMsg.password}</span>
                  )}
                  <button className="cx-button" type="submit" onClick={navigate('/Dashboard')}>
                    Submit
                  </button>
                  {/* </form> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}
