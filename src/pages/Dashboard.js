import React,{useContext, useEffect, useState} from "react";
import ApiConfig from "../api/ApiConfig";
import { simpleGetCallWithErrorResponse } from "../api/ApiServices";
import "../assets/css/cx-dashboard.css";
import { AppContext } from "../context/AppContext";
import SpinnerCmp from "../SpinnerCmp";
export default function Dashboard() {
  const { sidebar, setSidebar } = useContext(AppContext);
const [data,setdata]=useState({})
const [loading, setLoading] = useState(false);








  return (
    <main>
      {
        loading ?
        <div className="sweet-loading">
      
        <SpinnerCmp   loading={loading}    
          size={40} />
      </div>: <div className={sidebar ? " cx-container cx-container-inactive" : "cx-container"}>
        <div className="row">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <div className="cxd-card">
              <h2>ssss</h2>
              <label>Admins</label>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 53.068 43.918"
              >
                <path
                  id="Union_7"
                  data-name="Union 7"
                  d="M39.4,43.918a1.165,1.165,0,0,1-1.16-1.17V40.88a9.4,9.4,0,0,1-2.2-1.3l-1.618.942a1.155,1.155,0,0,1-1.585-.427l-2.319-4.047a1.177,1.177,0,0,1,.423-1.6l1.591-.927a9.066,9.066,0,0,1,0-2.6l-1.591-.927a1.18,1.18,0,0,1-.423-1.6l2.319-4.047a1.163,1.163,0,0,1,.7-.545,1.15,1.15,0,0,1,.88.118l1.617.942a9.4,9.4,0,0,1,2.2-1.3V21.7a1.165,1.165,0,0,1,1.16-1.17h4.639a1.165,1.165,0,0,1,1.16,1.17v1.868a9.31,9.31,0,0,1,2.2,1.3l1.619-.942a1.154,1.154,0,0,1,1.584.427l2.32,4.047a1.178,1.178,0,0,1-.424,1.6l-1.591.927a9.066,9.066,0,0,1,0,2.6l1.591.927a1.18,1.18,0,0,1,.423,1.6L50.6,40.1a1.159,1.159,0,0,1-1.584.427l-1.619-.942a9.4,9.4,0,0,1-2.2,1.3v1.868a1.165,1.165,0,0,1-1.16,1.17Zm-2.321-11.7a4.639,4.639,0,1,0,4.639-4.678A4.664,4.664,0,0,0,37.075,32.222ZM0,43.917v-2.31A16.131,16.131,0,0,1,16.044,25.426h9.167a15.955,15.955,0,0,1,3.236.331,11.756,11.756,0,0,0,2.982,18.157v0ZM10.313,10.4A10.315,10.315,0,1,1,20.628,20.8,10.37,10.37,0,0,1,10.313,10.4Z"
                />
              </svg>
            </div>
          </div>
          <div className="col-sm-6 col-md-4 col-lg-3">
            <div className="cxd-card">
              <h2>{"sfedgrg"}</h2>
              <label>Users</label>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                style={{ fill: "rgba(0, 0, 0, 1)" }}
              >
                <path d="M7.5 6.5C7.5 8.981 9.519 11 12 11s4.5-2.019 4.5-4.5S14.481 2 12 2 7.5 4.019 7.5 6.5zM20 21h1v-1c0-3.859-3.141-7-7-7h-4c-3.86 0-7 3.141-7 7v1h17z"></path>
              </svg>
            </div>
          </div>
          {/* <div className="col-sm-6 col-md-4 col-lg-3">
            <div className="cxd-card">
              <h2>1</h2>
              <label>Account</label>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                style={{ fill: "rgba(0, 0, 0, 1)" }}
              >
                <path d="M20 2H8a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2zm-6 2.5a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5zM19 15H9v-.25C9 12.901 11.254 11 14 11s5 1.901 5 3.75V15z"></path>
                <path d="M4 8H2v12c0 1.103.897 2 2 2h12v-2H4V8z"></path>
              </svg>
            </div>
          </div> */}
          {/* <div className="col-sm-6 col-md-4 col-lg-3">
            <div className="cxd-card">
              <h2>10</h2>
              <label>Settings</label>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                style={{ fill: "rgba(0, 0, 0, 1)" }}
              >
                <path d="m2.344 15.271 2 3.46a1 1 0 0 0 1.366.365l1.396-.806c.58.457 1.221.832 1.895 1.112V21a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-1.598a8.094 8.094 0 0 0 1.895-1.112l1.396.806c.477.275 1.091.11 1.366-.365l2-3.46a1.004 1.004 0 0 0-.365-1.366l-1.372-.793a7.683 7.683 0 0 0-.002-2.224l1.372-.793c.476-.275.641-.89.365-1.366l-2-3.46a1 1 0 0 0-1.366-.365l-1.396.806A8.034 8.034 0 0 0 15 4.598V3a1 1 0 0 0-1-1h-4a1 1 0 0 0-1 1v1.598A8.094 8.094 0 0 0 7.105 5.71L5.71 4.904a.999.999 0 0 0-1.366.365l-2 3.46a1.004 1.004 0 0 0 .365 1.366l1.372.793a7.683 7.683 0 0 0 0 2.224l-1.372.793c-.476.275-.641.89-.365 1.366zM12 8c2.206 0 4 1.794 4 4s-1.794 4-4 4-4-1.794-4-4 1.794-4 4-4z"></path>
              </svg>
            </div>
          </div> */}
        </div>
      </div>

      }
     
    </main>
  );
}
