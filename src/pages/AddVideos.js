import React from "react";
import "../assets/css/style.css";

export default function AddVideos() {
  return (
    <main>
      <header>
        <ul
          className="nav nav-tabs d-flex align-items-center"
          id="cx-tab"
          role="tablist"
        >
          <div className="cx-add-btn">
            <button type="button" className="cx-btn">
              <i className="bx bx-plus"></i>Add Topic
            </button>
            <button
              type="button"
              className="cx-btn cxc-add-folder"
              data-bs-toggle="modal"
              data-bs-target="#cxc-am"
            >
              <i className="bx bxs-folder-open"></i>
            </button>
          </div>
          <li className="nav-item" role="presentation">
            <button
              className="nav-link active"
              id="home-tab"
              data-bs-toggle="tab"
              data-bs-target="#cxc-l1"
              type="button"
              role="tab"
              aria-controls="home"
              aria-selected="true"
            >
              Level 1
            </button>
          </li>
          <li className="nav-item" role="presentation">
            <button
              className="nav-link"
              id="profile-tab"
              data-bs-toggle="tab"
              data-bs-target="#cxc-l2"
              type="button"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
            >
              Level 2
            </button>
          </li>
          <li className="nav-item" role="presentation">
            <button
              className="nav-link"
              id="profile-tab"
              data-bs-toggle="tab"
              data-bs-target="#cxc-l3"
              type="button"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
            >
              Level 3
            </button>
          </li>
        </ul>
      </header>
      <div className="cx-container">
        <div className="tab-content" id="myTabContent">
          <div
            className="tab-pane fade show active"
            id="cxc-l1"
            role="tabpanel"
            aria-labelledby="home-tab"
          >
            <div className="cxc-folder">
              <div className="cxc-folder-title">
                <h4>Whats App</h4>
                <div className="cxc-title-btns">
                  <button
                    className=""
                    type="button"
                    data-bs-toggle="modal"
                    data-bs-target="#cxc-qm"
                  >
                    <i className="bx bx-x"></i>
                  </button>
                  <button
                    className=""
                    type="button"
                    data-bs-toggle="modal"
                    data-bs-target="#cxc-em"
                  >
                    <i className="bx bxs-pencil"></i>
                  </button>
                  <button className="" type="button">
                    <i className="bx bx-plus"></i>
                  </button>
                </div>
              </div>

              {/* Accordian Starts  */}
              <div className="accordion" id="accordionEx">
                <div className="accordion-item cxc-acc-item">
                  <div className="cxc-card-title">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFour"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Mobile Data
                    </button>
                  </div>
                  <div
                    id="collapseFour"
                    className="accordion-collapse collapse cxc-acc-collapse"
                    aria-labelledby="headingFour"
                    data-bs-parent="#accordionEx"
                  >
                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-tname" className="form-label">
                            Topic Name
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-tname"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-vlink" className="form-label">
                            Video Link
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-vlink"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Icon Familiarization</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Matching Pairs</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name</h4>
                            <button
                              className="cxc-add-icons"
                              type="button"
                              data-bs-toggle="modal"
                              data-bs-target="#cxc-ca"
                            >
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Drag and Drop</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i>Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-12">
                        <div className="cxc-assess-footer">
                          <button className="cxc-delete cx-btn" type="button">
                            Delete
                          </button>
                          <div className="cxc-group">
                            <label htmlFor="cxc-order" className="form-label">
                              Order
                            </label>
                            <select
                              className="form-select"
                              id="cxc-order"
                              aria-label="Default select example"
                            >
                              <option selected>Select Order</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="accordion-item cxc-acc-item">
                  <div className="cxc-card-title">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFive"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Auto Rotate
                    </button>
                  </div>
                  <div
                    id="collapseFive"
                    className="accordion-collapse collapse cxc-acc-collapse"
                    aria-labelledby="headingFour"
                    data-bs-parent="#accordionEx"
                  >
                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-tname" className="form-label">
                            Topic Name
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-tname"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-vlink" className="form-label">
                            Video Link
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-vlink"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Icon Familiarization</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Matching Pairs</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Drag and Drop</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-12">
                        <div className="cxc-assess-footer">
                          <button className="cxc-delete cx-btn" type="button">
                            Delete
                          </button>
                          <div className="cxc-group">
                            <label htmlFor="cxc-order" className="form-label">
                              Order
                            </label>
                            <select
                              className="form-select"
                              id="cxc-order"
                              aria-label="Default select example"
                            >
                              <option selected>Select Order</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Accordian Ends  */}
            </div>

            {/* Accordian Starts */}
            <div className="accordion" id="accordionExample">
              <div className="accordion-item">
                <div className="cxc-card-title">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    Torch
                  </button>
                </div>
                <div
                  id="collapseOne"
                  className="accordion-collapse collapse cxc-acc-collapse"
                  aria-labelledby="headingFour"
                  data-bs-parent="#accordionEx"
                >
                  <div className="cxc-video-o">
                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-tname" className="form-label">
                            Topic Name
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-tname"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-vlink" className="form-label">
                            Video Link
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-vlink"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="accordion-item">
                <div className="cxc-card-title">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    Bluetooth
                  </button>
                </div>
                <div
                  id="collapseTwo"
                  className="accordion-collapse collapse cxc-acc-collapse"
                  aria-labelledby="headingFour"
                  data-bs-parent="#accordionEx"
                >
                  <div className="cxc-video-o">
                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-tname" className="form-label">
                            Topic Name
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-tname"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <label htmlFor="cxc-vlink" className="form-label">
                            Video Link
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="cxc-vlink"
                            placeholder="Untitled"
                          />
                        </div>
                      </div>

                      <div className="col-lg-12">
                        <div className="cxc-assess-footer">
                          <button className="cxc-delete cx-btn" type="button">
                            Delete
                          </button>
                          <div className="cxc-group">
                            <label htmlFor="cxc-order" className="form-label">
                              Order
                            </label>
                            <select
                              className="form-select"
                              id="cxc-order"
                              aria-label="Default select example"
                            >
                              <option selected>Select Order</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="accordion-item">
                <div className="cxc-card-title">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseThree"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    Icon Familiarization
                  </button>
                </div>
                <div
                  id="collapseThree"
                  className="accordion-collapse collapse cxc-acc-collapse"
                  aria-labelledby="headingFour"
                  data-bs-parent="#accordionEx"
                >
                  <div className="cxc-iconset">
                    {/*Add Icons  */}
                    <div className="cxc-icon-des">
                      <div className="cx-btn-grp">
                        <button
                          className="cx-btn"
                          data-bs-toggle="modal"
                          data-bs-target="#cxc-if"
                        >
                          Add/Edit Icons
                        </button>
                      </div>
                      <div className="row">
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Add Icons */}

                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Matching Pairs</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Drag and Drop</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 01</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 02</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 03</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 04</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      {/* <div className="col-lg-12">
                                            <div className="cxc-assess-footer">
                                                <button className="cxc-delete cx-btn" type = "button">Delete</button>
                                                <div className="cxc-group"> 
                                                    <label htmlFor="cxc-order" className="form-label">Order</label>
                                                    <select className="form-select" id="cxc-order" aria-label="Default select example">
                                                        <option selected>Select Order</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> */}
                    </div>
                  </div>

                  <div className="cxc-iconset">
                    {/*  Add Icons */}
                    <div className="cxc-icon-des">
                      <div className="cx-btn-grp">
                        <button className="cx-btn">Add/Edit Icons</button>
                      </div>
                      <div className="row">
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3">
                          <div className="cxc-icons">
                            <img src="images/curriculum/img_holder.png" />
                            <div className="cxc-icon-detail">
                              <h5>Icon Name</h5>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Morbi hendrerit elit ex, eget
                                ornare arcu semper eu. Suspendisse rutrum
                                volutpat urna ut egestas.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Add Icons */}

                    <div className="row cxc-assess">
                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Matching Pairs</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Drag and Drop</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 01</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 02</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 03</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 cxc-assess-col">
                        <div className="cxc-group">
                          <div
                            className="d-flex align-items-center justify-content-between"
                            style={{ marginBottom: "10px" }}
                          >
                            <h4>Correct Name 04</h4>
                            <button className="cxc-add-icons" type="button">
                              <i className="bx bx-plus"></i> Add/Edit Icons
                            </button>
                          </div>
                          <div className="cxc-tags">
                            <ul>
                              <li>Torch</li>
                              <li>Battery</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi</li>
                              <li className="cxc-more">
                                <i className="bx bx-dots-horizontal-rounded"></i>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      {/*  <div className="col-lg-12">
                                        <div className="cxc-assess-footer">
                                            <button className="cxc-delete cx-btn" type = "button">Delete</button>
                                            <div className="cxc-group"> 
                                                <label htmlFor="cxc-order" className="form-label">Order</label>
                                                <select className="form-select" id="cxc-order" aria-label="Default select example">
                                                    <option selected>Select Order</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>       */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- Accordian Ends --> */}
          </div>
          <div
            className="tab-pane fade show active"
            id="cxc-l2"
            role="tabpanel"
            aria-labelledby="home-tab"
          ></div>
          <div
            className="tab-pane fade show active"
            id="cxc-l3"
            role="tabpanel"
            aria-labelledby="home-tab"
          ></div>
        </div>
      </div>
    </main>
  );
}
