import React, {useContext, useEffect, useState} from "react";
import swal from "sweetalert";
import ApiConfig from "../api/ApiConfig";
import { postWithAuthCallWithErrorResponse, simpleGetCallWithErrorResponse, simplePostCall } from "../api/ApiServices";
import "../assets/css/cx-admin-listing.css";
import { AppContext } from "../context/AppContext";
import ReactTooltip from 'react-tooltip';
import SpinnerCmp from "../SpinnerCmp";
export default function AdminList() {
  const { sidebar, setSidebar } = useContext(AppContext);





  const [userList, setUserList] = useState({});

  const [loading, setLoading] = useState(false);










  return (
    <main>
      <header>
     

        <ul
          className="nav nav-tabs d-flex align-items-center"
          id="cx-tab"
          role="tablist"
        >
          {/* <div className="cx-search">
            <input
              type="text"
              className="cx-form-control"
              placeholder="Search..."
              style={{
                border: "1px solid #e7e7e7",
                marginLeft: "15px",
                paddingLeft: "15px",
                height: "36px",
                borderRadius: "5px",
              }}
            />
          </div> */}
          {/* <div className="cx-add-btn">
            <button type="button" className="cx-btn">
              <i className="bx bx-plus"></i>Add Admin
            </button>
          </div> */}
        </ul>
      </header>
      {
        loading? 
        <div className="sweet-loading">
      
        <SpinnerCmp   loading={loading}    
          size={40} />
      </div>:
       <div className={sidebar ? " cx-container cx-container-inactive" : "cx-container"}>
       <div className="row" >
         <div className="col-lg-12 table-overflow" >
           <div className="table-responsive" data-simplebar >
             <table className="table" id="cxu-table" >
               <thead>
                 <tr>
                   <th>SL.NO.</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Phone Number</th>
                   <th>Actions</th>
                 </tr>
               </thead>
               <tbody>
               <tr>
                         <td>{ 1}</td>
                         <td>{"defdf"}</td>
                         <td>{"sdefef"}</td>
                         <td>{"jabdjwnd"}</td>
                         {/* <td></td> */}
                         <td>
                           <ul>
                             <li>
                               <button
                                 type="button"
                                
                               >
                                 <i className="bx bx-x" data-tip="remove from admin list"></i>
                               </button>
                             </li>
                             {/* <li>
                               <button type="button">
                                 <i className="bx bxs-show"></i>
                               </button>
                             </li> */}
                         
                             <ReactTooltip />
                             
                           </ul>
                         </td>
                       </tr>
                 
               
               </tbody>
             </table>
           </div>
         </div>
       </div>
       <nav aria-label="Page navigation example" id="cx-pagination">
         <ul className="pagination">
           <li className="page-item">
             <a className="page-link" href="#">
               Previous
             </a>
           </li>
           <li className="page-item">
             <a className="page-link" href="#">
               1
             </a>
           </li>
           <li className="page-item">
             <a className="page-link" href="#">
               2
             </a>
           </li>
           <li className="page-item">
             <a className="page-link" href="#">
               3
             </a>
           </li>
           <li className="page-item">
             <a className="page-link" href="#">
               Next
             </a>
           </li>
         </ul>
       </nav>
     </div>
      }
     
    </main>
  );
}
