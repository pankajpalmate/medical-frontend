import React, { useContext, useEffect, useState } from "react";
import ApiConfig from "../api/ApiConfig";
import {
  postWithAuthCallWithErrorResponse,
  simpleGetCallWithErrorResponse,
  simplePostCall,
} from "../api/ApiServices";
import "../assets/css/cx-admin-listing.css";
import { AppContext } from "../context/AppContext";
import swal from "sweetalert";
import ReactTooltip from 'react-tooltip';
import SpinnerCmp from "../SpinnerCmp";
export default function UserList() {
  const { sidebar, setSidebar, customerData } = useContext(AppContext);
  const [userList, setUserList] = useState({});

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getUserList();
  }, []);

  const getUserList = () => {
    setLoading(true);
    simpleGetCallWithErrorResponse(ApiConfig.USER_LIST).then((res) => {
      console.log(res);
      setUserList(res.json.data);
      setLoading(false);
    });
  };

  const userApprove = (id) => {
    simplePostCall(
      ApiConfig.USER_APPROVE,
      JSON.stringify({
        user_id: id,
      })
    )
      .then((res) => {
        console.log(res);
        getUserList();
        swal({
          title: "success",
          text: "user approved successfully",
          icon: "success",
          button: true,
        })
      })
      .catch((err) => {
        console.log(err);
      });
  };
  
  const deleteuser = (id) => {
    postWithAuthCallWithErrorResponse(
      ApiConfig.DELETE_USER,
    {
        user_id: id,
      }
    )
      .then((res) => {
        console.log(res);
        getUserList();
        if(res.json){
          swal({
            title: "success",
            text: res.json.message,
            icon: "success",
            button: true,
          })
        }
       
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const makeadmin = (id) => {
    postWithAuthCallWithErrorResponse(
      ApiConfig.MAKE_ADMIN,
    {
        user_id: id,
      }
    )
      .then((res) => {
        console.log(res);
        getUserList();
        if(res.json){
          swal({
            title: "success",
            text: res.json.message,
            icon: "success",
            button: true,
          })
        }
       
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <main>
      <div
        className={
          sidebar ? " cx-container cx-container-inactive" : "cx-container"
        }
      >{
        loading ? <div className="sweet-loading">
      
        <SpinnerCmp   loading={loading}    
          size={40} />
      </div>

        :
        <div className="row">
        <div className="col-lg-12 table-overflow">
          <div className="table-responsive" data-simplebar>
            <table className="table" id="cxu-table">
              <thead>
                <tr>
                  <th>SL.NO.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {userList ? (
                  userList.length &&
                  userList.map((list, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{list.user_name}</td>
                        <td>{list.user_email_id}</td>
                        <td>{list.user_phone_no}</td>
                        {/* <td></td> */}
                        <td>
                          <ul>
                            <li>
                              <button
                                type="button"
                                onClick={() => userApprove(list.user_id)}
                                data-tip="approve this user"
                              >
                                <i className="bx bxs-check-circle" ></i>
                              </button>
                            </li>
                            {/* <li>
                              <button type="button">
                                <i className="bx bxs-show"></i>
                              </button>
                            </li> */}
                            <li>
                            <ReactTooltip />
                              <button type="button"
                              onClick={()=>{deleteuser(list.user_id)}}
                              data-tip="delete this user"
                              >
                                 <i className="bx bxs-trash-alt"></i>
                              </button>
                             
                            </li>
                            {
                              list.user_role =='admin'?"":
                              <li>
                          
                              <button type="button"
                              onClick={()=>{makeadmin(list.user_id)}}
                              data-tip="make admin"
                              >
                                 <i className="bx bxs-wrench"></i>
                              </button>
                             
                            </li>
                            }
                         
                          </ul>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <span className="text-center text-danger">
                    no data found
                  </span>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      }
      
        <nav aria-label="Page navigation example" id="cx-pagination">
          <ul className="pagination">
            <li className="page-item">
              <a className="page-link" href="#">
                Previous
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                1
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                2
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                3
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                Next
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </main>
  );
}
