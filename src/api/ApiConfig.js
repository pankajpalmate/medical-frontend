const BASE_URL = "https://glimmer.redbytes.in:3000/";

const ApiConfig = {
  BASE_URL,
  /* WebMarshall API url Start*/
  LOGIN_API: BASE_URL + "admin",
  USER_LIST: BASE_URL + "admin/users",
  USER_APPROVE: BASE_URL + "admin/approve",
  USER_DETAILS:BASE_URL + "admin/details",
  ADMIN_LIST:BASE_URL + "admin/adminlist",
  REMOVE_ADMIN:BASE_URL + "admin/removeadmin",
  MAKE_ADMIN:BASE_URL + "admin/makeadmin",
  DELETE_USER:BASE_URL + "admin/deleteUser"
};
export default ApiConfig;
