import { AppContext } from "./AppContext";

import React, { useState } from "react";
const AppState = (props) => {
  const [sidebar, setSidebar] = useState(false);
  const [Opensidebar, setOpensidebar] = useState(false);
  const [loading,setLoading]=useState(false)
  const [customerData, setCustomerData] = useState({
    user_id: localStorage.getItem("user_id")
      ? localStorage.getItem("user_id")
      : null,
    user_token: localStorage.getItem("user_token")
      ? localStorage.getItem("user_token")
      : null,
  });

  const [userDetails, SetUserDetails] = useState(
    localStorage.getItem("userDetails")
      ? JSON.parse(localStorage.getItem("userDetails"))
      : null
  );
  return (
    <div>
      <AppContext.Provider
        value={{
          sidebar,
          setSidebar,
          Opensidebar,
          setOpensidebar,
          customerData,
          setCustomerData,
          userDetails,
          SetUserDetails,
        }}
      >
        {props.children}
      </AppContext.Provider>
    </div>
  );
};

export default AppState;
